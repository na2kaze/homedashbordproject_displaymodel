# store in DB
import sys
import os
from scLogger import getLogger
import co2monitor
import switchbot
import smartplug
import amedasuapi
import forecastapi


def main():
    filename = os.path.basename(__file__.split(".")[0])
    log = getLogger(filename)
    
    threads = []
    

    ame = amedasuapi.AmedasuApi()
    threads.append(ame)

    co2 = co2monitor.Co2MonitorSensor()
    threads.append(co2)

    fore = forecastapi.ForecastApi()
    threads.append(fore)

    smtplg = smartplug.SmartPlugSensor()
    threads.append(smtplg)

    swt = switchbot.SwitchBotSensor()
    threads.append(swt)

    # 開始処理
    for t in threads:
        t.start()

    # 停止処理
    for thread in threads:
        if not thread.is_alive():
            log.error(thread.name + "は停止しました")
            #sys.exit()

if __name__ == "__main__":
    main()