import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

import requests
import json
import datetime
from models import *
import whetherCodeMst
from baseComp import BaseComp
from dbSetting import Session

class ForecastApi(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self.baseInterval = 900

    def doOnce(self):
        pass
    
    def loopBody(self):
        if datetime.datetime.now().hour == 5 or 11:
            res = self.getForecastRes('270000')

            if res.status_code == requests.codes.ok:
                #データjson化
                res = json.loads(res.text)
                
                # ワーク削除
                self.session = Session()
                self.session.query(Forecast).delete()

                # 初期化
                baseTD = self.createBaseTimeDefineList(res)
                addData = self.initAddData(8,baseTD[0])

                

                pops  = self.initDatetimeDict(baseTD,3,[0,6,12,18])

                #0 共通項目--------------------------------------------------------------------------------
                for i in range(len(addData)):
                    dt = datetime.datetime.strptime(baseTD[0], '%Y-%m-%dT%H:%M:%S%z') + datetime.timedelta(days=i)
                    key = dt.strftime('%Y-%m-%d')
                    addData[key].publishingOffice = res[0]['publishingOffice']
                    addData[key].reportDatetime = res[0]['reportDatetime']

                #1-1--------------------------------------------------------------------------------
                timeDefines = res[0]["timeSeries"][0]['timeDefines']
                areas       = res[0]["timeSeries"][0]['areas'][0]
                for i in range(len(timeDefines)):
                    key = datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')    
                    addData[key].weatherCode = areas['weatherCodes'][i]
                    addData[key].weather = areas['weathers'][i]
                    addData[key].wind = areas['winds'][i]
                    addData[key].wave = areas['waves'][i]

                #1-2--------------------------------------------------------------------------------
                timeDefines = res[0]["timeSeries"][1]['timeDefines']
                areas       = res[0]["timeSeries"][1]['areas'][0]
                for i in range(len(timeDefines)):
                    # areasの格納
                    pops[timeDefines[i]] = areas['pops'][i]

                # db格納
                start = 0
                end   = 4
                for i in baseTD:
                    key = datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')
                    addData[key].pops= list(pops.values())[start:end]
                    start += 4
                    end   += 4

                #1-3--------------------------------------------------------------------------------
                timeDefines = res[0]["timeSeries"][2]['timeDefines']
                areas       = res[0]["timeSeries"][2]['areas'][0]
                minTemp = '-'
                maxTemp = '-'
                for i in range(len(timeDefines)):
                    if datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').hour == 0:
                        key = datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')
                        addData[key].tempsMin = areas['temps'][i]
                        
                    elif datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').hour == 9:
                        key = datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')
                        addData[key].tempsMax = areas['temps'][i]

                #2-1--------------------------------------------------------------------------------
                timeDefines = res[1]['timeSeries'][0]['timeDefines']
                areas       = res[1]["timeSeries"][0]['areas'][0]
                for i in range(len(timeDefines)):
                    key = datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')
                    if areas['weatherCodes'][i] != "":
                        addData[key].weatherCode = areas['weatherCodes'][i]

                        # weatherName
                        for whether in whetherCodeMst.codelist:
                            if whether['code'] == areas['weatherCodes'][i]:
                                addData[key].weather = whether['name']
                            
                    if areas['pops'][i] != "":
                        addData[key].pops = areas['pops'][i]
                    if areas['reliabilities'][i] != "":
                        addData[key].reliabilities = areas['reliabilities'][i]

                #2-2--------------------------------------------------------------------------------
                timeDefines = res[1]['timeSeries'][1]['timeDefines']
                areas       = res[1]["timeSeries"][1]['areas'][0]
                for i in range(len(timeDefines)):
                    key = datetime.datetime.strptime(timeDefines[i], '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d')
                    if areas['tempsMin'][i] != "":
                        addData[key].tempsMin = areas['tempsMin'][i]
                    if areas['tempsMinUpper'][i] != "":
                        addData[key].tempsMinUpper = areas['tempsMinUpper'][i]
                    if areas['tempsMinLower'][i] != "":
                        addData[key].tempsMinLower = areas['tempsMinLower'][i]
                    if areas['tempsMax'][i] != "":
                        addData[key].tempsMax = areas['tempsMax'][i]
                    if areas['tempsMaxUpper'][i] != "":
                        addData[key].tempsMaxUpper = areas['tempsMaxUpper'][i]
                    if areas['tempsMaxLower'][i] != "":
                        addData[key].tempsMaxLower = areas['tempsMaxLower'][i]

                #commit--------------------------------------------------------------------------------
                try:
                    for addData in list(addData.values()):
                        self.session.add(addData)
                    self.session.commit()
                    self.log.info(addData)
                except:
                    self.session.rollback()
                    import traceback
                    self.log.error("ロールバックが発生しました。"+str(traceback.print_exc()))
                finally:
                    self.session.close()
            else:
                self.log.error("jsonデータ取得に失敗しました。")

    def getForecastRes(self,officeCode):
        """
        指定したオフィスコードの天気予報を返します。
        """
        URL = 'https://www.jma.go.jp/bosai/forecast/data/forecast/{0}.json'.format(officeCode) 
        res = requests.get(URL)
        return res

    # def getForecastDict(self,officeCode):
    #     """
    #     指定したオフィスコードの天気予報の情報を返します。
    #     """
    #     URL = 'https://www.jma.go.jp/bosai/forecast/data/forecast/{0}.json'.format(officeCode) 
    #     res = requests.get(URL).text
    #     if res.status_code == requests.codes.ok:
    #         resdict = json.loads(res)
    #     return resdict

    def initDatetimeDict(self,baseTD, days: int, timeSpan: list):
        """
        指定した日数と時間の辞書データを作ります。
        """
        returnDict = {}
        for i in range(days):
            dt = datetime.datetime.strptime(baseTD[0], '%Y-%m-%dT%H:%M:%S%z') + datetime.timedelta(days=i)

            for t in timeSpan:
                keydt = dt.replace(hour=t,minute=0,second=0)
                key   = keydt.strftime('%Y-%m-%dT%H:%M:%S%z')
                l = list(key)
                l.insert(-2,':')
                strky = str(''.join(l))
                returnDict[strky] = '-'
        return returnDict

    def createBaseTimeDefineList(self,res):
        """
        １つ目のtimeSeriesにあるTimeDefineをリストで返します。
        dbに格納するようのforecastクラスのkey名用のやつ。
        """
        baseTimeDefine = []
        timeDefines = res[0]["timeSeries"][0]['timeDefines']
        for i in range(len(timeDefines)):
            baseTimeDefine.append(timeDefines[i])
        return baseTimeDefine

    def initAddData(self,times, fastTimeDefine):
        addData = {}
        for i in range(times):
            dt = datetime.datetime.strptime(fastTimeDefine, '%Y-%m-%dT%H:%M:%S%z') + datetime.timedelta(days=i)
            key = dt.strftime('%Y-%m-%d')
            addData[key] = Forecast(
                createAt = datetime.datetime.now(),
                timeDefine = key
            )
        return addData

if __name__ == "__main__":
    ForecastApi().run()