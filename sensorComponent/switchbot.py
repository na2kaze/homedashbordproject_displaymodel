import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

from bluepy.btle import Scanner, DefaultDelegate
from datetime import datetime
from models import *
from baseComp import BaseComp

#Broadcastデータ取得用デリゲート
class SwitchbotScanDelegate(DefaultDelegate):
    #コンストラクタ
    def __init__(self, macaddr):
        #btle.DefaultDelegate.__init__(self)
        DefaultDelegate.__init__(self)
        #センサデータ保持用変数
        self.sensorValue = None
        self.macaddr = macaddr

    # スキャンハンドラー
    def handleDiscovery(self, dev, isNewDev, isNewData):
        # 対象Macアドレスのデバイスが見つかったら
        if dev.addr == self.macaddr:
            # アドバタイズデータを取り出し
            for (adtype, desc, value) in dev.getScanData():  
                #環境センサのとき、データ取り出しを実行
                if desc == '16b Service Data':
                    #センサデータ取り出し
                    self._decodeSensorData(value)

    # センサデータを取り出してdict形式に変換
    def _decodeSensorData(self, valueStr):
        #文字列からセンサデータ(4文字目以降)のみ取り出し、バイナリに変換
        valueBinary = bytes.fromhex(valueStr[4:])
        #バイナリ形式のセンサデータを数値に変換
        batt = valueBinary[2] & 0b01111111
        isTemperatureAboveFreezing = valueBinary[4] & 0b10000000
        temp = ( valueBinary[3] & 0b00001111 ) / 10 + ( valueBinary[4] & 0b01111111 )
        if not isTemperatureAboveFreezing:
            temp = -temp
        humid = valueBinary[5] & 0b01111111
        #dict型に格納
        self.sensorValue = {
            'SensorType': 'SwitchBot',
            'MacAddr': self.macaddr,
            'Temperature': temp,
            'Humidity': humid,
            'Battery': batt
        }

class SwitchBotSensor(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self.baseInterval = 3

    def doOnce(self):
        # センサーデータ取得
        args = "<SensorMacAddress>"
        # macアドレスは小文字にする
        self.macaddr = str.lower(args)

        # センサーデータ取得
        try:
            self.scanner = Scanner().withDelegate(SwitchbotScanDelegate(self.macaddr))
        except:
            pass
    
    def loopBody(self): 
        # macaddr指定した温湿度センサの値を取得
        try:
            self.scanner
        except:
            self.log.error("センサーとの疎通に失敗しました。再接続を試みます。")
            self.scanner = Scanner().withDelegate(SwitchbotScanDelegate(self.macaddr))

        try:
            self.scanner.scan( 5.0 )
        except:
            self.log.error("スキャンに失敗しました。")
            
        # db格納
        if self.scanner.delegate.sensorValue is not None:
            self.log.info(self.scanner.delegate.sensorValue)
            addData=SwitchBot( 
                createAt   = datetime.now(),
                MacAddr    = self.scanner.delegate.sensorValue['MacAddr'],
                Tempreture = self.scanner.delegate.sensorValue['Temperature'],
                Humidity   = self.scanner.delegate.sensorValue['Humidity'],
                Battery    = self.scanner.delegate.sensorValue['Battery']
                )
            self.storageData(addData)
            
if __name__ == "__main__":
    SwitchBotSensor().run()
