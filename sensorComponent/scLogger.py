import logging
import logging.handlers
from pathlib import Path

def getLogger(name, level=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    FORM = "%(asctime)s:	%(threadName)s:%(levelname)s:%(message)s"
    FILENAME = str(name) + ".log"
    SAVEPATH = Path.cwd() / Path(__file__).parents[0] / "log"

    # create file handle
    fh = logging.handlers.RotatingFileHandler(SAVEPATH / FILENAME , maxBytes=1000**2, backupCount=1)
    fh.setLevel(logging.DEBUG)
    fh_formatter = logging.Formatter(FORM)
    fh.setFormatter(fh_formatter)

    # create stream handle
    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    sh_formatter = logging.Formatter(FORM)
    sh.setFormatter(sh_formatter)

    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(sh)
    return logger

