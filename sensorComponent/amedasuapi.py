import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

import requests
import json
from datetime import datetime
from models import *
from baseComp import BaseComp

class AmedasuApi(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self.baseInterval = 15

    def doOnce(self):
        pass

    def loopBody(self):
        AREACODE = 62078
        res = self.getAmedasuRes()

        if res.status_code == requests.codes.ok:
            resdict = json.loads(res.text)
            targetData = resdict[str(AREACODE)]
        
            addData=Amedasu(
                createAt        = datetime.now(),
                pressure        = self.elementCheck('pressure',targetData),
                normalPressure  = self.elementCheck('normalPressure',targetData),
                temp            = self.elementCheck('temp',targetData),
                humidity        = self.elementCheck('humidity',targetData),
                sun10m          = self.elementCheck('sun10m',targetData),
                sum1h           = self.elementCheck('sum1h',targetData),
                precipitation10m= self.elementCheck('precipitation10m',targetData),
                precipitation1h = self.elementCheck('precipitation1h',targetData), 
                precipitation3h = self.elementCheck('precipitation3h',targetData),
                precipitation24h= self.elementCheck('precipitation24h',targetData),
                windDirection   = self.elementCheck('windDirection',targetData),
                wind            = self.elementCheck('wind',targetData)
                )
            self.storageData(addData)

    def getLatestTime(self):
        """
        サーバーに最新データがある時間を返します。
        """
        URL = 'https://www.jma.go.jp/bosai/amedas/data/latest_time.txt'
        latest_time = requests.get(URL).text
        tdatetime = datetime.strptime(latest_time, '%Y-%m-%dT%H:%M:%S%z')
        tdatetime = tdatetime.strftime('%Y%m%d%H%M%S')
        return tdatetime

    def getAmedasuRes(self):
        """
        指定したエリアコードのアメダスの情報を返します。
        """
        URL = 'https://www.jma.go.jp/bosai/amedas/data/map/{0}.json'.format(self.getLatestTime())  
        res = requests.get(URL)
        return res

    def elementCheck(self,keyname,targetData):
        if keyname in targetData and targetData[keyname] is not None:
            return targetData[keyname][0]
        else:
            return 0

if __name__ == "__main__":
    AmedasuApi().run()