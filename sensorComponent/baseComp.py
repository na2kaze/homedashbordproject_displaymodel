import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

import time
import threading
from scLogger import getLogger
from dbSetting import Session
import traceback
from contextlib import contextmanager

class BaseComp(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.baseInterval = 5
        self.nextInterval = self.baseInterval

    def __del__(self):
        self.log.info("終了")

    def loopBody(self):
        """
        ループ処理を定義します。
        """
        pass
    
    def doOnce(self):
        """
        ループ処理前に１回だけ実行したいものを定義します。
        """
        pass
            
    def storageData(self,addData):
        """
        データを格納する処理を定義します。
        """
        with self.session_scope() as session:
            session.add(addData)
    
    @contextmanager
    def session_scope(self):
        self.session = Session()  
        try:
            yield self.session 
            self.session.commit()
            self.log.info("コミット")
        except:
            self.session.rollback()  
            self.log.error("ロールバックが発生しました。"+str(traceback.print_exc()))
        finally:
            self.session.close()  

    def run(self):
        self.log = getLogger(self.filename)
        self.log.info("起動")
        try:
            self.doOnce()
        except Exception as e:
            self.log.error(str(e), exc_info=True)
            self.log.error(traceback.format_exc())
        while True:
            try:
                self.loopBody()
            except Exception as e:
                self.log.error(str(e), exc_info=True)
                self.log.error(traceback.format_exc())
                #raise
            ## intervalの処理。実行後にIntervalを初期化します。
            time.sleep(self.nextInterval)
            self.nextInterval = self.baseInterval

