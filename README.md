# HomeDashbordProject
各種センサー（温度計、湿度計、CO2、照度等）や天気情報を取得し、画面に表示するサイネージを作成するプロジェクトです。
※ディスプレイモデルです。

# インストール
## サービスをコピーする
```
sudo cp /home/pi/saine-zi/sensorComponent.service /lib/systemd/system/
sudo cp /home/pi/saine-zi/flask_app.service /lib/systemd/system/
```

## サービスを実行する
```
sudo systemctl start sensorComponent.service
sudo systemctl start flask_app.service
```

## サービスの確認
```
sudo systemctl status sensorComponent.service
sudo systemctl status flask_app.service
```

# 基本構成
![image](/uploads/2bc585164823cf2dc0fed452f667d4c3/image.png)

# 使用センサ

- CO2-mini
- SwitchBot Meter
- Gosund SmartPlug

# その他取得情報
- 気象庁 (天気予報に使用)
- ジョルダン (運行情報に使用)
