from flask import jsonify, Blueprint
from models import *
from app import session
from datetime import datetime, timedelta

switchbot = Blueprint('switchbot', __name__, template_folder='templates', static_folder='./static')

def checkValue(outputDict,key):
  if key in outputDict:
    return outputDict[key]
  else:
    return '---'

def switchbotFormat(output):
  return {
    #'Battery':{'name':'Battery','unit':'%','value':output['Battery']},
    'Humidity':{'name':'湿度','unit':'%','value':checkValue(output,'Humidity')},
    #'MacAddr':{'name':'MacAddr','unit':'','value':output['MacAddr']},
    'Tempreture':{'name':'気温','unit':'℃','value':checkValue(output,'Tempreture')}
    #'createAt':{'name':'createAt','unit':'','value':output['createAt']}
    }

def getSwitchBot():
  monitor = session.query(SwitchBot).\
      order_by(SwitchBot.createAt.desc()).\
      filter(CO2mini.createAt >= datetime.now() - timedelta(minutes=1)).\
      first()
  monitor_schema = SwitchBotSchema(many=False)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'
  return sutatus,output

@switchbot.route("/api/switchbot", methods=["GET"])
def dbSwitchBot():
  sutatus, output = getSwitchBot()
  return jsonify({'switchbot': {'status':sutatus,'result':output}})