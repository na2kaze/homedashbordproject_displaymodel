from flask import jsonify, Blueprint
from models import *
from libs import *
import feedparser

yahoorss = Blueprint('yahoorss', __name__, template_folder='templates', static_folder='./static')

@yahoorss.route("/api/yahoorss", methods=["GET"])
def rssYahooNews():
    RSS_URL = 'https://news.yahoo.co.jp/rss/topics/top-picks.xml'
    d = feedparser.parse(RSS_URL)
    returnlist = []
    for entry in d.entries:
        returnlist.append({'title':entry.title,'link':entry.link})
    return jsonify({'yahoorss': returnlist})
