from flask import jsonify, Blueprint
from models import *
from app import session
from libs import *
import json

forecast = Blueprint('forecast', __name__, template_folder='templates', static_folder='./static')

def forecastFormat(output):
  for result in output:
    result["pops"] = "".join(result["pops"].replace(",","/").replace("{","").replace("}",""))
  return output

def getForecast():
  monitor = session.query(Forecast)
  monitor_schema = ForecastSchema(many=True)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'
  return sutatus,output

@forecast.route("/api/forecast", methods=["GET"])
def dbForecast():
  sutatus, output = getForecast()

  return jsonify({'forecast': {'status':sutatus,'result':output}})
