from flask import jsonify, Blueprint
from models import *
from app import session

traininfo = Blueprint('traininfo', __name__, template_folder='templates', static_folder='./static')

def getTrainInfo():
  targetList = ["東海道新幹線","大阪メトロ中央線","大阪メトロ御堂筋線"]

  monitor = session.query(TrainInfo).\
      filter(TrainInfo.Name.in_(targetList)).\
      all()
  monitor_schema = TrainInfoSchema(many=True)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'

  #障害情報が出ているか
  findlist = []
  # 情報があるものをリストにまとめる
  for info in output:
      if info['Name'] in targetList:
          findlist.append(info['Name'])
          info['announced'] = True

  # 差分を求めて情報のないものをリスト化
  falselist = list(set(targetList) - set(findlist))

  for info in falselist:
      output.append({
          'Name':info,
          'announced':False,
          'Status':'平常運転',
          'Comment':'事故・遅延情報はありません'})

  return sutatus, output

@traininfo.route("/api/traininfo", methods=["GET"])
def dbTrainInfo():
  sutatus, output = getTrainInfo()
  return jsonify({'traininfo': {'status':sutatus,'result':output}})