from flask import jsonify, Blueprint
from libs import *

dashbord = Blueprint('dashbord', __name__, template_folder='templates', static_folder='./static')

import controllers

@dashbord.route("/api/dashbord", methods=["GET"])
def dashbordapi():
    amedasuStatus, amedasuResult = controllers.amedasu.getAmedasu()
    co2miniStatus, co2miniResult = controllers.co2mini.getCO2mini()
    forecastStatus, forecastResult = controllers.forecast.getForecast()
    smartplugStatus, smartplugResult = controllers.smartplug.getSmartplug("678013645002911999fb")
    switchbotStatus, switchbotResult = controllers.switchbot.getSwitchBot()
    traininfoStatus, traininfoResult = controllers.traininfo.getTrainInfo()

    return jsonify({
        'amedasu': {'status':amedasuStatus,'result':controllers.amedasu.amedasuFormat(amedasuResult)},
        'co2mini': {'status':co2miniStatus,'result':controllers.co2mini.CO2miniFormat(co2miniResult)},
        'forecast': {'status':forecastStatus,'result':controllers.forecast.forecastFormat(forecastResult)},
        'smartplug': {'status':smartplugStatus,'result':controllers.smartplug.smartplugFormat(smartplugResult)},
        'switchbot': {'status':switchbotStatus,'result':controllers.switchbot.switchbotFormat(switchbotResult)},
        'traininfo': {'status':traininfoStatus,'result':traininfoResult}
    })
