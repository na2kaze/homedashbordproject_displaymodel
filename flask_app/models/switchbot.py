from sqlalchemy import Column, Integer, String, Float, DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class SwitchBot(Base):
    __tablename__ = 'switchbot'
    createAt   = Column(DateTime, primary_key=True)
    MacAddr    = Column(String, primary_key=True)
    Tempreture = Column(Float)
    Humidity   = Column(Integer)
    Battery    = Column(Integer)

class SwitchBotSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = SwitchBot
        load_instance = True