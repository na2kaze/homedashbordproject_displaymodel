from sqlalchemy import Column, Integer, String, Float, DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class Smartplug(Base):
    __tablename__ = 'smartplug'
    createAt    = Column(DateTime, primary_key=True)
    deviceId    = Column(String)
    Current     = Column(Integer)
    Power       = Column(Integer)
    Voltage     = Column(Integer)

class SmartplugSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Smartplug
        load_instance = True