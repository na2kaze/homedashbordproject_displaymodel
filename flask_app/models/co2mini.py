from sqlalchemy import Column, Integer, String, Float, DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class CO2mini(Base):
    __tablename__ = 'co2mini'
    createAt    = Column(DateTime, primary_key=True)
    co2         = Column(Integer)
    temperature = Column(Float)

class CO2miniSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = CO2mini
        load_instance = True